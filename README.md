## Project setup
```
npm install http-server -g
npm install
```
## Run project
```
http-server
navigate to /dist
```
## Update changes to project
```
npx webpack --config webpack.config.js
