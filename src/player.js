const Gameboard = require('./Gameboard');
const Ship = require('./Ship');

const player =((playerNum,gamestart,playerBoard,opponentBoard) => {
  let shipLengths = [5,4,3,3,2];
    const placeShip = (length,x,y,isRotate,oldX,oldY) => {
      const ship = Ship(playerNum,length);
      if(!isRotate)
      {
      if(shipLengths.indexOf(length)!=-1)
      {

        const attemptedShipPlacement = playerBoard.placeShip(ship,x,y,isRotate);
        if(attemptedShipPlacement!="You can't place a ship here")
        {
          shipLengths.splice(shipLengths.indexOf(length),1);
        }
        return attemptedShipPlacement;
      }
      return "Please pick a different piece";
      }
      else {
        return playerBoard.placeShip(ship,x,y,isRotate,oldX,oldY)
      }
    }
    const placeShipVertical = (length,x,y,oldX,oldY) => {
      const ship = Ship(playerNum,length);
      return playerBoard.placeShipVertical(ship,x,y,oldX,oldY);
    }
    const attack = (x,y) => {
      return opponentBoard.receiveAttack(x,y);
    }
    return {placeShip,attack,placeShipVertical};
});
module.exports = player;
