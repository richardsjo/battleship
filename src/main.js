// import * from "../src/gameboard.js";
const Gameboard = require('./gameboard');
const Player = require('./Player');
const Ai = require('./Ai');
const Ship = require('./Ship');


let gameStart = true;
const gameboardPlayer = Gameboard();
const playerGameboard = document.querySelector('.playerGameboard');
const aiGameboard = document.querySelector('.aiGameboard');
const AiGameboard = Gameboard();
const AI = Ai(2,gameStart,AiGameboard,gameboardPlayer);
const player = Player(1,gameStart,gameboardPlayer,AiGameboard);
let numberOfPlacedShips = 0;
let chosenShip = null;
let isShipChosen = false;
let moveChosen = false;
let positionClicked = null;
const ships = {carrier: 5,
                battleship: 4,
                cruiser: 3,
                submarine: 3,
                destroyer: 2};
let playerUnits=5;
let aiUnits=5;
let movementInfo = {ship:null,orientation:null,startPosition:null};

for(let i=0;i<10;i++)
{
  const div = document.createElement('div');
  div.setAttribute("id",'row'+i);
  div.style.display = 'inline-block';
  playerGameboard.appendChild(div);
  for(let j=0;j<10;j++)
  {
    const div = document.createElement('div');
    div.classList.add('empty');
    div.setAttribute("id",String(i)+String(j));
    const selectedDiv = document.querySelector('#row'+i);
    selectedDiv.appendChild(div);
    document.getElementById(String(i)+String(j)).addEventListener('click',function clicker(e) {
      if(isShipChosen)
      {
        let placeShipAttempt = player.placeShip(ships[chosenShip],parseInt(e.srcElement.id[0]),parseInt(e.srcElement.id[1]),false);
        if(placeShipAttempt!="You can't place a ship here" && placeShipAttempt!="Please pick a different piece")
        {
          makeShip(ships[chosenShip],e.srcElement.id);
          isShipChosen=false;
          numberOfPlacedShips++;
          chosenShip=null;
          document.querySelector(".shipChosen").style.display="none";
        }
      }
      else {
        document.getElementById("options").style.display="inline-block";
        if(!moveChosen)
        {
          positionClicked=e.srcElement.id;
        }
        if(gameboardPlayer.getBoard()[e.srcElement.id])
          {
          let shipLength = gameboardPlayer.getBoard()[e.srcElement.id].ship.getLength();
            for(let i=0;i<10;i++)
            {
              for(let j=0;j<10;j++)
              {
                if(gameboardPlayer.getBoard()[String(i)+String(j)])
                {
                  if(gameboardPlayer.getBoard()[String(i)+String(j)].ship.getLength()==shipLength &&
                gameboardPlayer.getBoard()[String(i)+String(j)].position==1)
                  {
                    movementInfo.startPosition = String(i)+String(j);
                    movementInfo.ship = gameboardPlayer.getBoard()[e.srcElement.id].ship;
                  }
                  if(gameboardPlayer.getBoard()[String(i)+String(j)].position==2 && parseInt(movementInfo.startPosition[0])<i)
                  {
                    movementInfo.orientation="horizontal";
                  }
                  if(gameboardPlayer.getBoard()[String(i)+String(j)].position==2 && parseInt(movementInfo.startPosition[1])<j)
                  {
                    movementInfo.orientation="vertical";
                  }
                }
              }
            }
          }
        if(moveChosen)
        {
          console.log(movementInfo.orientation)
          console.log(movementInfo.startPosition);
          document.getElementById("options").style.display="none";
          moveChosen=false;
          if(movementInfo.orientation=="horizontal")
          {
            let placeShipAttempt = player.placeShip(movementInfo.ship.getLength(),parseInt(e.srcElement.id[0]),parseInt(e.srcElement.id[1]),true,parseInt(movementInfo.startPosition[0]),parseInt(movementInfo.startPosition[1]));
            if(placeShipAttempt!="You can't place a ship here" && placeShipAttempt!="Please pick a different piece")
            {
              resetShip(movementInfo.ship.getLength(),movementInfo.startPosition);
              makeShip(movementInfo.ship.getLength(),e.srcElement.id);
            }
          }
          else {
            let placeShipAttempt =player.placeShipVertical(movementInfo.ship.getLength(),parseInt(e.srcElement.id[0]),parseInt(e.srcElement.id[1]),parseInt(movementInfo.startPosition[0]),parseInt(movementInfo.startPosition[1]));
            if(placeShipAttempt!="You can't place a ship here")
            {
              resetShipVertical(movementInfo.ship.getLength(),movementInfo.startPosition);
              makeShipVertical(movementInfo.ship.getLength(),e.srcElement.id);
              document.getElementById("options").style.display="none";
            }
          }
        }
      }
    })
  }
}

const shipsArray = ['carrier','battleship','cruiser','submarine','destroyer'];
function makeShip(length,startingSquare) {
  for(let i=0;i<length;i++)
  {
    const position = document.getElementById((parseInt(startingSquare[0])+i)+startingSquare[1]);
    position.classList.remove('empty');
    position.classList.add('ship');
  }
}
function makeShipVertical(length,startingSquare) {
  for(let i=0;i<length;i++)
  {
    const position = document.getElementById(startingSquare[0]+(parseInt(startingSquare[1])+i));
    position.classList.remove('empty');
    position.classList.add('ship');
  }
}
function resetShip(length,startingSquare) {
  for(let i=0;i<length;i++)
  {
    const position = document.getElementById((parseInt(startingSquare[0])+i)+startingSquare[1]);
    position.classList.remove('ship');
    position.classList.add('empty');
  }
}
function resetShipVertical(length,startingSquare) {
  for(let i=0;i<length;i++)
  {
    const position = document.getElementById(startingSquare[0]+(parseInt(startingSquare[1])+i));
    position.classList.remove('ship');
    position.classList.add('empty');
  }
}
function createShip(length,chosenDiv) {
  for(let i=0;i<length;i++)
  {
    const div = document.createElement('div');
    div.classList.add('ship');
    div.style.display = "inline-block";
    const selectedDiv = document.querySelector("."+chosenDiv);
    selectedDiv.appendChild(div);
  }
}




for(let i=0;i<shipsArray.length;i++)
{
  createShip(ships[shipsArray[i]],shipsArray[i]);
  document.querySelector("."+shipsArray[i]).addEventListener('click',function(e) {
    if(gameStart)
    {
    const div = document.querySelector(".shipChosen");
    chosenShip = event.srcElement.parentNode.className;
    div.textContent="You have selected "+chosenShip[0].toUpperCase()+chosenShip.slice(1,chosenShip.length);
    createShip(ships[chosenShip],"shipChosen");
    div.style.display="block";
    isShipChosen=true;
  }
  });
}

function attemptedHit(attack,idName,whoAttacks) {
  if(attack=="You have already attacked here.")
  {
    alert("You have already attacked here.")
  }
  else if (attack=="miss")
  {
    document.getElementById(idName).classList.remove('empty');
    document.getElementById(idName).classList.add('miss');
  }
  else if(attack=="hit")
  {
    document.getElementById(idName).classList.remove('empty');
    document.getElementById(idName).classList.add('hit');
  }
  else if(attack=="Ship sunk")
  {
    document.getElementById(idName).classList.remove('empty');
    document.getElementById(idName).classList.add('hit');
    alert("Ship sunk!");
    // alert("name add thing, still need to do")
    if(whoAttacks=="player")
    {
      aiUnits--;
    }
    else {
      playerUnits--;
    }
    if(playerUnits==0)
    {
      alert("AI wins")
    }
    else if(aiUnits==0)
    {
      alert("Player wins")
    }
  }
}


document.getElementById("start").addEventListener('click',function(e) {
  if(numberOfPlacedShips==5)
  {
    gameStart=false;
    document.querySelector(".pieces").style.display="none";
    document.querySelector(".shipChosen").style.display="none";
    for(let i=0;i<10;i++)
    {
      const div = document.createElement('div');
      div.setAttribute("id",'aiRow'+i);
      div.style.display = 'inline-block';
      aiGameboard.appendChild(div);
      for(let j=0;j<10;j++)
      {
        const div = document.createElement('div');
        div.classList.add('empty');
        div.setAttribute("id","AI"+String(i)+String(j));
        const selectedDiv = document.querySelector('#aiRow'+i);
        selectedDiv.appendChild(div);
        // document.getElementById(String(i)+String(j)).removeEventListener('click',clicker);
        const oldDiv = document.getElementById(String(i)+String(j))
        let oldClass = oldDiv.className;
        oldDiv.classList.remove(oldClass);
        oldDiv.classList.add("empty");
        div.addEventListener('click',function (e) {
        let attackPosition = e.srcElement.id;
        let attack = player.attack(parseInt(attackPosition[2]),parseInt(attackPosition[3]));
        let attackResult = attemptedHit(attack,attackPosition,'player');
        if(attack!="You have already attacked here.")
        {
          let result = AI.attack();
          attemptedHit(result[1],String(result[0].x)+String(result[0].y),'ai');
        }
        });
      }
    }
    for(let i=0;i<5;i++)
    {
      AI.placeShip();
    }
  }
  else {
    alert("Please place all of your pieces.");
  }
})

document.getElementById("rotate").addEventListener('click',function(e) {
  if(movementInfo.orientation=="horizontal")
  {
    let placeShipAttempt =player.placeShipVertical(movementInfo.ship.getLength(),parseInt(movementInfo.startPosition[0]),parseInt(movementInfo.startPosition[1]));
    if(placeShipAttempt!="You can't place a ship here")
    {
      resetShip(movementInfo.ship.getLength(),movementInfo.startPosition);
      makeShipVertical(movementInfo.ship.getLength(),movementInfo.startPosition);
      document.getElementById("options").style.display="none";
    }
  }
  else {
    let placeShipAttempt =player.placeShip(movementInfo.ship.getLength(),parseInt(movementInfo.startPosition[0]),parseInt(movementInfo.startPosition[1]),true);
    if(placeShipAttempt!="You can't place a ship here")
    {
      resetShipVertical(movementInfo.ship.getLength(),movementInfo.startPosition);
      makeShip(movementInfo.ship.getLength(),movementInfo.startPosition);
      document.getElementById("options").style.display="none";
    }
  }
})

document.getElementById("move").addEventListener('click',function(e) {
    moveChosen=true;
    let shipLength = gameboardPlayer.getBoard()[positionClicked].ship.getLength();
      for(let i=0;i<10;i++)
      {
        for(let j=0;j<10;j++)
        {
          if(gameboardPlayer.getBoard()[String(i)+String(j)])
          {
            if(gameboardPlayer.getBoard()[String(i)+String(j)].ship.getLength()==shipLength &&
          gameboardPlayer.getBoard()[String(i)+String(j)].position==1)
            {
              movementInfo.startPosition = String(i)+String(j);
              movementInfo.ship = gameboardPlayer.getBoard()[positionClicked].ship;
            }
            if(gameboardPlayer.getBoard()[String(i)+String(j)].position==2 && parseInt(movementInfo.startPosition[0])<i)
            {
              movementInfo.orientation="horizontal";
            }
            if(gameboardPlayer.getBoard()[String(i)+String(j)].position==2 && parseInt(movementInfo.startPosition[1])<j)
            {
              movementInfo.orientation="vertical";
            }
          }
        }
      }
})
