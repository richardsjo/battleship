const Ship = require('./Ship');

describe('Ship', function () {

  it('should be an object', function () {
    const ship = Ship(1,3);
    expect(typeof ship).toBe('object');
  });

  it('changes hit area to true and returns string when hit in same place', function () {
    const ship = Ship(1,3);
    ship.hit(2);
    expect(ship.hit(2)).toEqual("Already hit here");
  })
  it('returns the fact it is sunk when all parts of it are hit and only then', function () {
    let ship = Ship(1,2);
    ship.hit(1);
    expect(ship.hasItSunk()).toEqual(false);
    ship.hit(2);
    expect(ship.hasItSunk()).toEqual(true);
  })
  it('returns the passed in length', function () {
    const ship = Ship(1,2);
    expect(ship.getLength()).toEqual(2);
  })
  it('returns the object describing where the ship has been hit', function () {
    const ship = Ship(1,2);
    whereHitObject = ship.getWhereHit();
    expect(whereHitObject['1']).toEqual(false);
    expect(whereHitObject['2']).toEqual(false);
  })
});
